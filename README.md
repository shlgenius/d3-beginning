<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html">
        <!--<titl> D3 beginning </title>-->
    </head>
    <body>
         <!--  D3 강의 학습 -->
        <h3>D3 beginning</h3>
        <hr><br>
        <h4> Basic HTML Page </h4>
        <h4> SVG Examples; CSS Examples; JavaScript Examples; D3 Examples </h4>
        <br><hr><br>
        <h5>Tool</h5>
        <h5>Visual Studio Code</h5>
        <br><hr><br>
        <h5>Reference</h5>
        <h5>https://github.com/curran/screencasts/tree/gh-pages/introToD3</h5>
        <h5>https://www.youtube.com/watch?v=8jvoTV54nXw</h5>
    </body>
</html>